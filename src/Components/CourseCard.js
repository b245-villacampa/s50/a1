import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {Row, Col} from 'react-bootstrap';
import {useState} from 'react';
import {useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';
import {useParams} from 'react-router-dom';


export default function CourseCard({courseProp}){

	


	const {_id, name, description, price} = courseProp;
	// The "course" in the CourseCard component is called a "prop" which is a shorthand for property

	// The curly braces are used for proos to signify that we are providing information using expression


	// Ise the state hook for this component to be able to store it's state
	// States are used to keep track of information related to individual components 

		// const [getter, setter] = useState(initialGetterValue);

	const [enrollees, setEnrollees] = useState(0);
	const [availableSeats, setAvailableSeats] = useState(30);
	//add new statie that will declare whether the button is disable  
	const [isDisabled, setIsDisabled] = useState(false);

	// Initial Value of enrollees state

	console.log(enrollees);

	// if you want to change/reassign the value of the state

		// setEnrollees(1);
		//console.log(enrollees);
	const {user} = useContext(UserContext);
	
		function enroll(){
			if(availableSeats > 1){
				setEnrollees(enrollees+1);
				setAvailableSeats(availableSeats-1);
			}else{
				setEnrollees(enrollees+1);
				setAvailableSeats(availableSeats-1);
			 	alert("No more available Seats!")
			}
		}
	
	// Define a "useEffect" hook to have the "CourseCard" component do perform certain task

	// This will run automatically
		// Syntax:
			// useEffect(sideEffect/function, [dependencies]);

	// sideEffect/function- it will run on the first load and will reload depending on the dependencies

		useEffect(()=>{
			if(availableSeats === 0){
				setIsDisabled(true);
			}
		}, [availableSeats]);


	return(
		<Row className="mt-3">
			<Col>
				<Card>
				    <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text> 
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>Php {price}</Card.Text>
				        <Card.Subtitle>Enrollees:</Card.Subtitle>
				        <Card.Text>{enrollees}</Card.Text>
				        <Card.Subtitle>Available Seats:</Card.Subtitle>
				        <Card.Text>{availableSeats}</Card.Text>    
				        {
				        	user ?
				        	<Button as = {Link} to ={`/course/${_id}`} 
				        	onClick={enroll} 
				        	disabled={isDisabled}> See More Details!</Button>
				        	:
				        	<Button as = {Link} to = "/login">Login</Button>
				        }
				        
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}