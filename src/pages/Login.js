import {Form, Button} from 'react-bootstrap';
import {Fragment} from 'react';
import {useEffect, useContext, useState} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2';

export default function Login(){

	const navigate = useNavigate();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(false)
	//const[user, setUser] = useState(localStorage.getItem("email"));

	// Allows us to consume the user contaxt and it's properties for user verification

	const {user, setUser} = useContext(UserContext);
	console.log(user);

	useEffect(()=>{
		if(email !== '' && password !== ''){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	}, [email, password])

	function login(event){
		event.preventDefault()

		// if you want to add the email of the authenticated user in the local storage 
			// Syntax:
				//localStorage.setItem("propertyName", value) 


		/*Process a fetch request to corresponsing backend API*/
		// Syntax: fetch('url', {options});

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method:'POST',
			headers:{
				'Content-Type':'application/json'
			},
			body:JSON.stringify({
				email:email,
				password:password
			})
		}).then(result => result.json()).then(data =>{
			console.log(data);

			if(data === false){
				Swal.fire({
					title:"Authentication Failed",
					icon:"error",
					text:"Please try again!"
				})
			}else{
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'));

				Swal.fire({
					title:"Authentication Successful",
					icon:"success",
					text:"Welcome to Zuitt!"
				})
				navigate('/');
			}
		})


		// localStorage.setItem("email", email);
		// setUser(localStorage.getItem("email"));
		// alert('Login successful!')
		// setEmail('');
		// setPassword('');
		// navigate("/")
	}

	const retrieveUserDetails = (token) =>{
		// The token sent as part of the request's header information	

		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers:{
				Authorization:`Bearer ${token}`
			}
		}).then(result => result.json()).then(data =>{
			console.log(data);
			
			setUser({
				id:data._id,
				isAdmin:data.isAdmin
			});
		})
	}


	return(
		
		user ?
		<Navigate to = "/*" />
		:
		<Fragment>
			<h1 className="mt-3">Login</h1>
			<Form onSubmit={event => login(event)}>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
				    type="email" 
				    placeholder="Enter email" 
				    value={email}
				    onChange={event => setEmail(event.target.value)}
				    required/>
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
				    type="password" 
				    placeholder="Password" 
				    value = {password}
				    onChange={event => setPassword(event.target.value)}
				    required/>
			  </Form.Group>
			  
			  {
			  	isDisabled?
			  	<Button variant="secondary" type="submit" disabled>
			    	Submit
			  	</Button>
			  	:
			  	<Button variant="success" type="submit">
			    	Submit
			  	</Button>
			  }
			  
			</Form>
		</Fragment>
	);

}