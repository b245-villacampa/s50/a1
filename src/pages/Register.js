import {Form, Button} from 'react-bootstrap';
import {Fragment} from 'react';

// import the hook taht are neede in our page
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2';

export default function Register(){
	// Create 3 new state where we will store the value from input of the email, password and confirmPassword

	const [firstName, setFirstName] = useState(" ");
	const [lastName, setLastName] = useState(" ");
	const [mobileNo, setMobileNo] = useState(" ");
	const [email, setEmail] = useState(" ");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const navigate = useNavigate();
	const {user, setUser} = useContext(UserContext);  
	//const[user, setUser] = useState(localStorage.getItem("email"));
	// Create another state for the button 
	const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		if(firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password !== "" && confirmPassword !== "" && password === confirmPassword){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNo, email, password, confirmPassword])

	// useEffect(()=>{
	// 	console.log(email);
	// 	console.log(password);
	// 	console.log(confirmPassword);
	// }, [email, password, confirmPassword]);

	function register(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method:"POST",
			headers:{
				'Content-Type':'application/json'
			}, 
			body:JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email:email,
				password:password
			})
		}).then(result => result.json()).then(data => {
			if(data){
				Swal.fire({
					title:"Congratulation, you are now registered on our website!",
					icon:"success",
					text:"Please try to login!"
				})

				navigate("/login");
			}else{
				Swal.fire({
					title:"Registration Failed!",
					icon:"error",
					text:"Please try to again!"
				})
			}
		})

		

		// setEmail("")
		// setPassword("")
		// setConfirmPassword("")
		// setFirstName("")
		// setLastName("")
		// setMobileNo("")
		
		// navigate("/")
	}

	return(
		user ?
		<Navigate to = "/*" />
		:
		<Fragment>
			<h1 className="text-center mt-3">Register</h1>
			<Form className="mt-5 form mx-auto" onSubmit = {event => register(event)}>
			   
			    <Form.Group className="mb-3" controlId="formBasicFirstName">
			        <Form.Label className="text-primary">First Name:</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Enter First Name"
			        	value = {firstName}
			        	onChange = {event => setFirstName(event.target.value)}
			        	required />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicLastName">
			        <Form.Label className="text-primary">Last Name:</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Enter Last Name"
			        	value = {lastName}
			        	onChange = {event => setLastName(event.target.value)}
			        	required />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicMobileNo">
			        <Form.Label className="text-primary">Mobile Number:</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Enter Mobile Number"
			        	value = {mobileNo}
			        	onChange = {event => setMobileNo(event.target.value)}
			        	required />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label className="text-primary">Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email"
			        	value = {email}
			        	onChange = {event => setEmail(event.target.value)}
			        	required />
			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label className="text-primary">Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password"
			        	value = {password}
			        	onChange ={event=> setPassword(event.target.value)} 
			        	required />
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
			        <Form.Label className="text-primary">Confirm Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Confirm your Password" 
			        	value = {confirmPassword}
			        	onChange ={event=> setConfirmPassword(event.target.value)}
			        	required />
			    </Form.Group>

			    {

			    	isActive ?
			    	<Button variant="primary" type="submit">
			        Submit
			    	</Button>
			    	:
			    	<Button variant="danger" type="submit" disabled>
			        Submit
			    	</Button>
			    }
			      
			    
			</Form>
		</Fragment>
		)
}