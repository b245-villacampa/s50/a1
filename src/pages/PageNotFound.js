import {Link} from 'react-router-dom';
import {Fragment} from 'react';
import myImage from '../Images/404-error.png'

export default function PageNotFound(){
	return(
		<Fragment>
			<div className="text-center">
				<img src={myImage}></img>
			</div>
			<h1 className="text-center">Page Not Found</h1>
			<p className="text-center">Go back to the <Link to = "/">homepage</Link></p>
		</Fragment>

	)
}